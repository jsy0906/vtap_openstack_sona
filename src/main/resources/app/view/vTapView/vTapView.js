// js for sample app custom view
(function () {
    'use strict';

    // injected refs
    var $log, $scope, wss, ks, fs, wss_t;

    // constants
//    var dataReq = 'sampleCustomDataRequest',
//        dataResp = 'sampleCustomDataResponse',
//        testReq = 'sampleTableDetailsRequest';
    var dataReq = 'vTapViewDataRequest',
    	dataResp = 'vTapViewDataResponse',
    	testReq = 'sampleTableDetailsRequest';
    
    function addKeyBindings() {
        var map = {
            space: [getData, 'Fetch data from server'],

            _helpFormat: [
                ['space']
            ]
        };

        ks.keyBindings(map);
    }

    function getData() {
    	wss.sendEvent(dataReq);
    }

    function respDataCb(data) {
    	
    	$log.log('test: ' + data);
    	
        $scope.data = data;
        $scope.$apply();
    }
    
    function applyPolicy() {
    	
    	$scope.policy = {};
    	
    	if(!$scope.dev_id && !$scope.dev_port) {
	    	$scope.policy = { 
	    						policy: {
	    							index: -1,	
	    							type: "flow",
					    			mon_mac_addr: $scope.mon_mac_addr,
					    			dev_id		: $scope.dev_id,
					    			dev_port	: $scope.dev_port,
					    			src_mac_addr: $scope.src_mac_addr, 
					    			dst_mac_addr: $scope.dst_mac_addr,
					    			protocol	: $scope.protocol,
					    			src_port_num: $scope.src_port_num,
					    			dst_port_num: $scope.dst_port_num
	    						}    	
			    			};
    	} else if ($scope.dev_id && $scope.dev_port){
    		$scope.policy = { 
					policy: {
						index: -1,	
						type: "port",
		    			mon_mac_addr: $scope.mon_mac_addr,
		    			dev_id		: $scope.dev_id,
		    			dev_port	: $scope.dev_port,
		    			src_mac_addr: $scope.src_mac_addr, 
		    			dst_mac_addr: $scope.dst_mac_addr,
		    			protocol	: $scope.protocol,
		    			src_port_num: $scope.src_port_num,
		    			dst_port_num: $scope.dst_port_num
					}    	 
    			};
    	}
    	
    	wss.sendEvent(dataReq, $scope.policy); 

    	// JS binded variable reset
    	$scope.mon_mac_addr = undefined;
    	$scope.dev_id = undefined; 
    	$scope.dev_port = undefined;
    	$scope.src_mac_addr = undefined;
    	$scope.dst_mac_addr = undefined;
    	$scope.protocol = undefined;
    	$scope.src_port_num = undefined;
    	$scope.dst_port_num = undefined;
    	
    	// HTML item reset
    	document.getElementById('mon_mac_addr').value = '';
    	document.getElementById('dev_id').value = '';
    	document.getElementById('dev_port').value = '';
    	document.getElementById('src_mac_addr').value = '';
    	document.getElementById('dst_mac_addr').value = '';
    	document.getElementById('src_port_num').value = '';
    	document.getElementById('dst_port_num').value = '';

    }
    
    function removePolicy(policy, index) {

        $scope.policy = {};
        
        if(policy.dev_id != null && policy.dev_port != null) {
	        $scope.policy = {        					
	        					policy: {
	        						index: index,		
	        						type: "port",
					    			mon_mac_addr: policy.mon_mac_addr,
					    			dev_id		: policy.dev_id,
					    			dev_port	: policy.dev_port,
					    			src_mac_addr: policy.src_mac_addr,
					    			dst_mac_addr: policy.dst_mac_addr,
					    			protocol	: policy.protocol,
					    			src_port_num: policy.src_port_num,
					    			dst_port_num: policy.dst_port_num
	        					}				    			
	        				};  
        } else {
        	$scope.policy = {        					
					policy: {
						index: index,		
						type: "flow",
		    			mon_mac_addr: policy.mon_mac_addr,
		    			dev_id		: policy.dev_id,
		    			dev_port	: policy.dev_port,
		    			src_mac_addr: policy.src_mac_addr,
		    			dst_mac_addr: policy.dst_mac_addr,
		    			protocol	: policy.protocol,
		    			src_port_num: policy.src_port_num,
		    			dst_port_num: policy.dst_port_num
					}				    			
				};  
        }
        
        console.log($scope.policy);
        wss.sendEvent(dataReq, $scope.policy);
     }
    
    angular.module('ovVTapView', [])
        .controller('OvVTapViewCtrl',
        ['$log', '$scope', 'WebSocketService', 'KeyService', 'TableBuilderService', 'FnService',

        function (_$log_, _$scope_, _wss_, _ks_, tbs, _fs_) {
            $log = _$log_;
            $scope = _$scope_;
            wss = _wss_;
            ks = _ks_;
            fs = _fs_;

            var handlers = {};
            $scope.data = {};
            $scope.panelDetails = {};

            // TableBuilderService creating a table for us
            tbs.buildTable({
                scope: $scope,
                tag: 'vTapTable'
            });

            // data response handler
            handlers[dataResp] = respDataCb;
            wss.bindHandlers(handlers);

            addKeyBindings();

            $scope.applyPolicy = applyPolicy;
            $scope.removePolicy = removePolicy;
            
            // cleanup
            $scope.$on('$destroy', function () {
                wss.unbindHandlers(handlers);
                ks.unbindKeys();
                $log.log('OvVTapViewCtrl has been destroyed');
            });

            $log.log('OvVTapViewCtrl has been created');
            
        }]);

}());
