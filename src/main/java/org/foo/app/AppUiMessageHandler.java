/*
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.foo.app;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableSet;

import org.onlab.osgi.DefaultServiceDirectory;
import org.onlab.osgi.ServiceDirectory;
import org.onlab.packet.MacAddress;
import org.onosproject.net.DeviceId;
import org.onosproject.net.PortNumber;
import org.onosproject.ui.RequestHandler;
import org.onosproject.ui.UiMessageHandler;
import org.onosproject.ui.table.TableModel;
import org.onosproject.ui.table.TableRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;

/**
 * Skeletal ONOS UI Custom-View message handler.
 */
public class AppUiMessageHandler extends UiMessageHandler {
	
	// for sampleCustom
//    private static final String SAMPLE_CUSTOM_DATA_REQ = "sampleCustomDataRequest";
//    private static final String SAMPLE_CUSTOM_DATA_RESP = "sampleCustomDataResponse";
	private static final String SAMPLE_CUSTOM_DATA_REQ = "vTapViewDataRequest";
	private static final String SAMPLE_CUSTOM_DATA_RESP = "vTapViewDataResponse";
     
    private final Logger log = LoggerFactory.getLogger(getClass());
    
	private static final String SAMPLE_TABLE_DETAIL_REQ = "sampleTableDetailsRequest";
	private static final String SAMPLE_TABLE_DETAIL_RESP = "sampleTableDetailsResponse";
	private static final String DETAILS = "details";

	// for sampleTable
//	private static final String SAMPLE_TABLE_DATA_REQ = "sampleTableDataRequest";
//	private static final String SAMPLE_TABLE_DATA_RESP = "sampleTableDataResponse";
//	private static final String SAMPLE_TABLES = "sampleTables";
	
	private static final String SAMPLE_TABLE_DATA_REQ = "vTapTableDataRequest";
	private static final String SAMPLE_TABLE_DATA_RESP = "vTapTableDataResponse";
	private static final String SAMPLE_TABLES = "vTapTables";

	private static final String NO_ROWS_MESSAGE = "No items found";

	private static final String POLICY_ID = "id";
	private static final String MON_MAC_ADDR = "mon_mac_addr";
	private static final String DEV_ID = "dev_id";
	private static final String DEV_PORT = "dev_port";
	private static final String SRC_MAC_ADDR = "src_mac_addr";
	private static final String DST_MAC_ADDR = "dst_mac_addr";
	private static final String PROTOCOL = "protocol";
	private static final String SRC_PORT_NUM = "src_port_num";
	private static final String DST_PORT_NUM = "dst_port_num";

	private static final String[] COLUMN_IDS =
			{ POLICY_ID, MON_MAC_ADDR, DEV_ID, DEV_PORT, SRC_MAC_ADDR, DST_MAC_ADDR, PROTOCOL, SRC_PORT_NUM, DST_PORT_NUM };
    
    @Override
    protected Collection<RequestHandler> createRequestHandlers() {
        return ImmutableSet.of(
                new SampleCustomDataRequestHandler(),
                new SampleTableDataRequestHandler(),
				new SampleTableDetailRequestHandler()
        );
    }
    
    private final class SampleCustomDataRequestHandler extends RequestHandler {

        private SampleCustomDataRequestHandler() {
            super(SAMPLE_CUSTOM_DATA_REQ);
        }

        @Override
        public void process(ObjectNode payload) {

        	int index = -1;	
        	String type = null;
        	String mon_mac_addr = null;
        	String dev_id = null;
        	String dev_port = null;
	        String src_mac_addr = null;
	        String dst_mac_addr = null;	        
	        String protocol = null;
	        String src_port_num = null;
	        String dst_port_num = null; 	                	       	
	        	
	        try {
	        		
	        	JsonNode policy = payload.get("policy");
	        	log.info(policy.toString());
	        		
	        	if(policy.get("index") != null)
					index = policy.get("index").asInt();
	        	if(policy.get("type") != null)
					type = policy.get("type").asText();
	           	if(policy.get("mon_mac_addr") != null)
	           		mon_mac_addr = policy.get("mon_mac_addr").asText();
	           	if(policy.get("dev_id") != null)
	           		dev_id = policy.get("dev_id").asText();
	           	if(policy.get("dev_port") != null)
	           		dev_port = policy.get("dev_port").asText();
	        	if(policy.get("src_mac_addr") != null)
	        		src_mac_addr = policy.get("src_mac_addr").asText();
	           	if(policy.get("dst_mac_addr") != null)
	           		dst_mac_addr = policy.get("dst_mac_addr").asText();
	           	if(policy.get("protocol") != null) 
	           		protocol = policy.get("protocol").asText();
	           	if(policy.get("src_port_num") != null)
	           		src_port_num = policy.get("src_port_num").asText();
	           	if(policy.get("dst_port_num") != null)
	           		dst_port_num = policy.get("dst_port_num").asText();	        	
	        		
	        	ServiceDirectory serviceDirectory = new DefaultServiceDirectory();
	            AppComponent appComponentService = serviceDirectory.get(AppComponent.class);
	        	
		        int currentPolicyId = appComponentService.getCurrentPolicyId();
		        	
		        if(index == -1)
				{
		        	appComponentService.addTapPolicy(
		        					new TapPolicy(currentPolicyId,		
		        							type,
			       							mon_mac_addr,
			       							dev_id,
			       							dev_port,
			       							src_mac_addr, 
			       							dst_mac_addr,
			       							protocol, 
			       							src_port_num, 
			       							dst_port_num));
				}
				else {
						appComponentService.removeTapPolicy(index);
				}
		        	
	        } catch(Exception e) {
	        	log.error("error: ",e);
	        }
        	
        	ObjectNode result = objectNode();
        	result.put("response", 1);
        	sendMessage(SAMPLE_CUSTOM_DATA_RESP, result);
        }
    }
    
	// handler for sample table requests
	private final class SampleTableDataRequestHandler extends TableRequestHandler {

		private SampleTableDataRequestHandler() {
			super(SAMPLE_TABLE_DATA_REQ, SAMPLE_TABLE_DATA_RESP, SAMPLE_TABLES);
		}

		// if necessary, override defaultColumnId() -- if it isn't "id"
		@Override
		protected String defaultColumnId() {
			return POLICY_ID;
		}

		@Override
		protected String[] getColumnIds() {
			return COLUMN_IDS;
		}

		// if required, override createTableModel() to set column formatters / comparators

		@Override
		protected String noRowsMessage(ObjectNode payload) {
			return NO_ROWS_MESSAGE;
		}

		@Override
		protected void populateTable(TableModel tm, ObjectNode payload) {

			ServiceDirectory serviceDirectory = new DefaultServiceDirectory();
			AppComponent appComponentService = serviceDirectory.get(AppComponent.class);

			List<TapPolicy> policies = appComponentService.getTapPolicies();

			for(TapPolicy policy: policies) {
				populateRow(tm.addRow(), policy);
			}

		}

		private void populateRow(TableModel.Row row, TapPolicy policy) {
			
			row.cell(POLICY_ID, policy.getPolicyId())					
				.cell(MON_MAC_ADDR, policy.getMonMacAddr())
				.cell(DEV_ID, policy.getDevId())
				.cell(DEV_PORT, policy.getDevPort())
				.cell(SRC_MAC_ADDR, policy.getSrcMacAddr())
				.cell(DST_MAC_ADDR, policy.getDstMacAddr())
				.cell(PROTOCOL, policy.getProtocol())
				.cell(SRC_PORT_NUM, policy.getSrcPortNum())
				.cell(DST_PORT_NUM, policy.getDstPortNum());
		}
	}

	// handler for sample item details requests
	private final class SampleTableDetailRequestHandler extends RequestHandler {

		private SampleTableDetailRequestHandler() {
			super(SAMPLE_TABLE_DETAIL_REQ);
		}

		@Override
		public void process(ObjectNode payload) {

			log.info(payload.toString());
		}
	}    
    
    /*
     * Data model for a TAP policy
     */
    public static class TapPolicy {
    	
    	private int policyId;	// same as the index of the policy list in AppComponent => used for remove   
    	private String type;
    	private String mon_mac_addr;
    	private String dev_id;
    	private String dev_port;
    	private String src_mac_addr;
    	private String dst_mac_addr;
    	private String protocol;
    	private String src_port_num;
    	private String dst_port_num;
    	
    	private final Logger log = LoggerFactory.getLogger(getClass());
    	
    	TapPolicy(int policyId, String type, String mon_mac_addr, String dev_id, String dev_port,
    			String src_mac_addr, String dst_mac_addr, String protocol, String src_port_num, String dst_port_num) {
    		    		
    		this.policyId = policyId;   
    		this.type = type;
    		this.mon_mac_addr = mon_mac_addr;
    		this.dev_id = dev_id;
    		this.dev_port = dev_port;
    		this.src_mac_addr = src_mac_addr;
    		this.dst_mac_addr = dst_mac_addr;
    		this.protocol = protocol;
    		this.src_port_num = src_port_num;
    		this.dst_port_num = dst_port_num;
    	} 
    	 
    	int getPolicyId() {
    		return policyId;
    	}    	
    	
    	String getType() {
    		return type;
    	}
    	
    	MacAddress getMonMacAddr() {
    		if(mon_mac_addr != null) {
    			log.info(mon_mac_addr.toString());
    			return MacAddress.valueOf(mon_mac_addr);
    		}
    		else
    			return null;
    	}
    	
    	DeviceId getDevId() {
    		if(dev_id != null) {
    			log.info(dev_id.toString());
    			return DeviceId.deviceId(dev_id);
    		} 
    		else
    			return null;    		
    	}
    	
    	PortNumber getDevPort() {
    		if(dev_port != null) {
    			log.info(dev_port.toString());
    			return PortNumber.portNumber(dev_port);
    		}
    		else 
    			return null;    		
    	}
    	
    	MacAddress getSrcMacAddr() {
    		if(src_mac_addr != null) {
    			log.info(src_mac_addr.toString());
    			return MacAddress.valueOf(src_mac_addr);
    		}
    		else
    			return null;
    	}
    	
    	MacAddress getDstMacAddr() {
    		if(dst_mac_addr != null) {
    			log.info(dst_mac_addr.toString());
    			return MacAddress.valueOf(dst_mac_addr);
    		}
    		else
    			return null;
    	}
    	
    	String getProtocol() {
    		return protocol;
    	}
    	
    	String getSrcPortNum() {
    		return src_port_num;
    	}
    	
    	String getDstPortNum() {
    		return dst_port_num;
    	}
    }
}
