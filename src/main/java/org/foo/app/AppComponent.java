/*
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.foo.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.Service;
import org.foo.app.AppUiMessageHandler.TapPolicy;
import org.onlab.packet.Ethernet;
import org.onlab.packet.IPv4;
import org.onlab.packet.MacAddress;
import org.onlab.packet.TpPort;
import org.onlab.packet.VlanId;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.core.GroupId;
import org.onosproject.event.Event;
import org.onosproject.net.ConnectPoint;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.Host;
import org.onosproject.net.HostId;
import org.onosproject.net.Link;
import org.onosproject.net.Path;
import org.onosproject.net.PortNumber;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.flow.DefaultFlowRule;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.FlowEntry;
import org.onosproject.net.flow.FlowRule;
import org.onosproject.net.flow.FlowRuleService;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.flow.TrafficTreatment.Builder;
import org.onosproject.net.flow.criteria.Criterion;
import org.onosproject.net.flow.criteria.EthCriterion;
import org.onosproject.net.flow.instructions.Instruction;
import org.onosproject.net.flow.instructions.Instructions;
import org.onosproject.net.group.DefaultGroupBucket;
import org.onosproject.net.group.DefaultGroupDescription;
import org.onosproject.net.group.DefaultGroupKey;
import org.onosproject.net.group.GroupBucket;
import org.onosproject.net.group.GroupBuckets;
import org.onosproject.net.group.GroupDescription;
import org.onosproject.net.group.GroupKey;
import org.onosproject.net.group.GroupService;
import org.onosproject.net.host.HostService;
import org.onosproject.net.intent.IntentService;
import org.onosproject.net.link.LinkEvent;
import org.onosproject.net.topology.TopologyEvent;
import org.onosproject.net.topology.TopologyListener;
import org.onosproject.net.topology.TopologyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

/**
 * Skeletal ONOS application component.
 */
@Component(immediate = true)
@Service(value = AppComponent.class)
public class AppComponent {	

    private final Logger log = LoggerFactory.getLogger(getClass());
    
    private ApplicationId appId;
    private static final String APP_ID = "org.foo.app";
    
    private List<TapPolicy> policies = new ArrayList<TapPolicy>();
    private int currentPolicyId = 1;
    
    private List<List<FlowRule>> listOfPolicyFlowRules = new ArrayList<List<FlowRule>>();
     
    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected GroupService groupService;
        
    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected FlowRuleService flowRuleService;
    
    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected CoreService coreService;
    
    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected DeviceService deviceService;
    
    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected TopologyService topologyService;
    
    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected HostService hostService;
    
    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected IntentService intentService;
    
    private final TopologyListener topologyListener = new InternalTopologyListener();    
    
    @Activate
    protected void activate() {
        log.info("AppComponent Started");
        
        appId = coreService.registerApplication(APP_ID);      
        
        topologyService.addListener(topologyListener);
    }

    @Deactivate
    protected void deactivate() {
    	
    	topologyService.removeListener(topologyListener);
    	groupService.purgeGroupEntries();    	
    	flowRuleService.removeFlowRulesById(appId);
    	
        log.info("Stopped");
    }
    
    public List<TapPolicy> getTapPolicies() {
    	return policies;
    }
    
    public int getCurrentPolicyId() {
    	return currentPolicyId;
    }
    
    // Add a flow TAP Policy
	public void addTapPolicy(TapPolicy policy) {
		
		currentPolicyId++;
    	policies.add(policy);
    	
    	if(policy.getType().equals("flow"))
    		apply(policy);
    	else if(policy.getType().equals("port"))
    		applyPortTap(policy);
	}
	
	private void applyPortTap(TapPolicy policy) {
		// TODO Auto-generated method stub
		try {
			
			Device targetDevice = deviceService.getDevice(policy.getDevId());
			log.info(String.format("Target device ID: %s", targetDevice.id()));
			
			PortNumber targetEgressPort = policy.getDevPort();
			log.info(String.format("Tagert port number: %s", targetEgressPort));
			
			Host monHost = hostService.getHost(HostId.hostId(policy.getMonMacAddr()));
	      	DeviceId monDevId = monHost.location().deviceId();		      	
	      	log.info(String.format("Monitor device ID: %s", monDevId.toString()));
	      	
	      	PortNumber egressPortToMonitor = PortNumber.NORMAL;		// egressPortToMonitorEdgeDevice
	      	
	      	Set<Path> monPaths = topologyService
	      			.getPaths(topologyService.currentTopology(), targetDevice.id(), monDevId);
	      	if(monPaths.isEmpty()) {
//		      		log.info("Monitor paths are empty");
//		      		Maybe Sender and Monitor are connected to the same edge switch
	      		log.info(String.format("Target device: %s /// Monitor edge device: %s", targetDevice.id(), monDevId));
	      		egressPortToMonitor = monHost.location().port();
	      	} else {
	      		Path monPath = monPaths.iterator().next();
	      		egressPortToMonitor = monPath.src().port();
	      	}
	      	
	      	installGroupAndRewriteFlowRule(targetDevice.id(), targetEgressPort, egressPortToMonitor, policy);
			
	      	PortNumber egressPortToMonitorHost = monHost.location().port();	      	
	      	installRecoveryFlowRule(monDevId, egressPortToMonitorHost, policy);
	      		      	
		} catch (Exception e) {    		
    		log.error("something error", e);    		
    		return;
    	}
	}

	/*
	 * To be modified:
	 * has to remove all the related flow rules installed by the policy
	 */
	public void removeTapPolicy(int index) {
        currentPolicyId--;
        
        remove(policies.get(index));
        
        policies.remove(index);
        
        // ...
    }
	
	private void remove(TapPolicy policy) {
		// TODO Auto-generated method stub
		
		List<FlowRule> flowRules = listOfPolicyFlowRules.get(policy.getPolicyId() - 1);
		log.info(String.format("\nRewriting rule: %s \nRecovery rule: %s", flowRules.get(0), flowRules.get(1)));
		
		flowRuleService.removeFlowRules(flowRules.get(0), flowRules.get(1));
	}

	// Apply a flow TAP Policy
    private void apply(TapPolicy policy) {
    	
    	try {
    		
	      	// Handle the bucket for Receiver
	      	Host dstHost = hostService.getHost(HostId.hostId(policy.getDstMacAddr()));
	      	DeviceId dstDevId = dstHost.location().deviceId();		      	
	      	log.info(String.format("Receiver device ID: %s", dstDevId.toString()));
	      	
	      	// Handle the bucket for Monitor
	      	Host monHost = hostService.getHost(HostId.hostId(policy.getMonMacAddr()));
	      	DeviceId monDevId = monHost.location().deviceId();		      	
	      	log.info(String.format("Monitor device ID: %s", monDevId.toString()));
    			      	
	      	Host srcHost = hostService.getHost(HostId.hostId(policy.getSrcMacAddr()));
	      	DeviceId srcDevId = srcHost.location().deviceId();
	      	
	      	PortNumber egressPortToReceiver = PortNumber.NORMAL;	// egressPortToReceiverEdgeDeivce
	      	PortNumber egressPortToMonitor = PortNumber.NORMAL;		// egressPortToMonitorEdgeDevice

	      	Set<Path> dstPaths = topologyService
	      			.getPaths(topologyService.currentTopology(), srcDevId, dstDevId);
	      	if(dstPaths.isEmpty()) {
//	      		log.info("Production paths are empty");
//	      		Maybe Sender and Receiver are connected to the same edge switch
	      		log.info(String.format("Sender edge device: %s /// Receiver edge device: %s", srcDevId, dstDevId));			      		
	      		egressPortToReceiver = dstHost.location().port();
	      		
	      	} else {
	      		Path dstPath = dstPaths.iterator().next();			// Just choose the firstly found path for now
	      		egressPortToReceiver = dstPath.src().port();
	      	}
	      	
	      	Set<Path> monPaths = topologyService
	      			.getPaths(topologyService.currentTopology(), srcDevId, monDevId);
	      	if(monPaths.isEmpty()) {
//		      		log.info("Monitor paths are empty");
//		      		Maybe Sender and Monitor are connected to the same edge switch
	      		log.info(String.format("Sender edge: %s /// Monitor edge: %s", srcDevId, monDevId));
	      		egressPortToMonitor = monHost.location().port();
	      	} else {
	      		Path monPath = monPaths.iterator().next();
	      		egressPortToMonitor = monPath.src().port();
	      	}
	      	
	      	List<FlowRule> policyFlowRules = new ArrayList<FlowRule>();
	      	
	      	// Install Rewriting rules with Group Table in the Sender edge device
	      	FlowRule rewritingRule = installGroupAndRewriteFlowRule(srcDevId, egressPortToReceiver, egressPortToMonitor, policy);
	      	policyFlowRules.add(0, rewritingRule);
	      	
	      	// Install Recovery rules in the Monitor edge device
      		PortNumber egressPortToMonitorHost = monHost.location().port();      		
	      	FlowRule recoveryRule = installRecoveryFlowRule(monDevId, egressPortToMonitorHost, policy);
	    	policyFlowRules.add(1, recoveryRule);
	      		      	
	    	listOfPolicyFlowRules.add(policyFlowRules);
	    	
	    	return;	    	
	    	
    	} catch (Exception e) {    		
    		log.error("something error", e);    		
    		return;
    	}
	}	
    
    // Install Rewriting rules for flow TAP policies
	private FlowRule installGroupAndRewriteFlowRule(DeviceId targetDeviceId, 
			PortNumber egressPortToReceiver, PortNumber egressPortToMonitor, TapPolicy policy) {
		
		try { 
			List<GroupBucket> buckets = Lists.newArrayList();
			
			// Bucket to receiver
	    	Builder builder = DefaultTrafficTreatment.builder();
	    	builder.setOutput(egressPortToReceiver);
	    	        	
	    	GroupBucket bucket = DefaultGroupBucket
	                .createAllGroupBucket(builder.build());
	        buckets.add(bucket);	  

	        // Bucket to monitor
	        builder = DefaultTrafficTreatment.builder();
	        
//	        Set<Host> hosts = hostService.getHostsByMac(policy.getMonMacAddr());
//	        Host devHost = hosts.iterator().next();
//	        builder.setIpDst(devHost.ipAddresses().iterator().next());	        
	        builder.setEthDst(policy.getMonMacAddr());	        
	        builder.setOutput(egressPortToMonitor);
//	        builder.setVlanId(VlanId.vlanId((short)policy.getPolicyId()));	// tag VLAN ID to identify mirrored traffic
	        
	    	bucket = DefaultGroupBucket
	                .createAllGroupBucket(builder.build());
	        buckets.add(bucket);
		        
	        final GroupKey key = new DefaultGroupKey((APP_ID + new Integer(policy.getPolicyId()).toString()).getBytes());
	        GroupDescription groupDesc = new DefaultGroupDescription(
	        		targetDeviceId, 
	        		GroupDescription.Type.ALL, 
	        		new GroupBuckets(buckets),
	        		key,
	        		policy.getPolicyId(),
	        		appId);
	        
	        groupService.addGroup(groupDesc);
	        // check a result of the addition
	        if(groupService.getGroup(targetDeviceId, key) == null) {
	        	log.info(String.format("This Group cannot be added to device: %s", targetDeviceId));
	        	return null;
	        }
	         
//	        Iterator<Group> itr = groupService.getGroups(targetDeviceId).iterator();
//	        while(itr.hasNext()) {
//	        	Group group = itr.next();
//	        	log.info(group.toString());
//	        }
	        
	        TrafficSelector.Builder selectorBuilder = DefaultTrafficSelector.builder();
	        
	        selectorBuilder.matchEthType(Ethernet.TYPE_IPV4);
	         
	        if(policy.getSrcMacAddr() != null)
	        	selectorBuilder.matchEthSrc(policy.getSrcMacAddr());
	        if(policy.getDstMacAddr() != null) 
	        	selectorBuilder.matchEthDst(policy.getDstMacAddr());
	        
	        if(policy.getProtocol() != null) {
		        if(policy.getProtocol().equals("TCP")) {
		        	selectorBuilder.matchIPProtocol(IPv4.PROTOCOL_TCP);
		        	
		        	if(policy.getSrcPortNum() != null)
		        		selectorBuilder.matchTcpSrc(TpPort.tpPort(Integer.parseInt(policy.getSrcPortNum())));
		        	if(policy.getDstPortNum() != null)
		        		selectorBuilder.matchTcpDst(TpPort.tpPort(Integer.parseInt(policy.getDstPortNum())));
		        	
		        } else if(policy.getProtocol().equals("UDP")){
		        	selectorBuilder.matchIPProtocol(IPv4.PROTOCOL_UDP);
		        	
		        	if(policy.getSrcPortNum() != null)
		        		selectorBuilder.matchUdpSrc(TpPort.tpPort(Integer.parseInt(policy.getSrcPortNum())));
		        	if(policy.getDstPortNum() != null)
		        		selectorBuilder.matchUdpDst(TpPort.tpPort(Integer.parseInt(policy.getDstPortNum())));
		        	
		        } else if(policy.getProtocol().equals("ICMP")){
		        	selectorBuilder.matchIPProtocol(IPv4.PROTOCOL_ICMP);
		        
		        } else if(policy.getProtocol().equals("Ethernet")) {
		        	
		        }
	        }
	        
	        TrafficSelector selector = selectorBuilder.build();
	        
	        TrafficTreatment treatment =  DefaultTrafficTreatment
	        		.builder().group(GroupId.valueOf(policy.getPolicyId())).build();
	        
	        FlowRule flowRule = DefaultFlowRule.builder()
	        						.forDevice(targetDeviceId)
	        						.withSelector(selector)
	        						.withTreatment(treatment)
	        						.withPriority(50000)
	        						.fromApp(appId)
	        						.withIdleTimeout(0)	        						
	        						.build();
	        
	        flowRuleService.applyFlowRules(flowRule);
	        
	        return flowRule;
	        
		} catch(Exception e) {
			log.error("someting error", e);
			return null;
		}
	}
	
	// Install Recovery rules for flow TAP policies
    private FlowRule installRecoveryFlowRule(DeviceId targetDeviceId, PortNumber egressPortToMonitorHost, TapPolicy policy) {
    		
    		try {
    			
    			TrafficSelector.Builder selectorBuilder = DefaultTrafficSelector.builder();
    			if(policy.getSrcMacAddr() != null)
    				selectorBuilder.matchEthSrc(policy.getSrcMacAddr());
    			if(policy.getMonMacAddr() != null)
    				selectorBuilder.matchEthDst(policy.getMonMacAddr());
    			
    			
//    			selectorBuilder.matchVlanId(VlanId.vlanId((short)policy.getPolicyId()));
    			
    			
    			TrafficSelector selector = selectorBuilder.build();
    			
    			TrafficTreatment treatment = DefaultTrafficTreatment.builder()
    											.setEthDst(policy.getDstMacAddr())	// At this time, a destinaton MAC address is required due to the src/dst address rewriting approach
    											.setOutput(egressPortToMonitorHost)
    											.build();
    			
    	        FlowRule flowRule = DefaultFlowRule.builder()
						.forDevice(targetDeviceId)
						.withSelector(selector)
						.withTreatment(treatment)
						.withPriority(50000)
						.fromApp(appId)
						.withIdleTimeout(0)
						.build();

    	        flowRuleService.applyFlowRules(flowRule);
    	        
    	        return flowRule;
    	        
    		} catch(Exception e) {
    			log.error("someting error", e);
    			return null;
    		}
    }	  
    
    /*
     * The following methods below are just copied from Reactive Forwarding App. 
     */    
    private class InternalTopologyListener implements TopologyListener {
        @Override
        public void event(TopologyEvent event) {
            List<Event> reasons = event.reasons();
            if (reasons != null) {
                reasons.forEach(re -> {
                    if (re instanceof LinkEvent) {
                        LinkEvent le = (LinkEvent) re;
                        if (le.type() == LinkEvent.Type.LINK_REMOVED) {
                            fixBlackhole(le.subject().src());
                        }
                    }
                });
            }
        }
    }
    
    private void fixBlackhole(ConnectPoint egress) {
        Set<FlowEntry> rules = getFlowRulesFrom(egress);
        Set<SrcDstPair> pairs = findSrcDstPairs(rules);

        Map<DeviceId, Set<Path>> srcPaths = new HashMap<>();

        for (SrcDstPair sd : pairs) {
            // get the edge deviceID for the src host
            Host srcHost = hostService.getHost(HostId.hostId(sd.src));
            Host dstHost = hostService.getHost(HostId.hostId(sd.dst));
            if (srcHost != null && dstHost != null) {
                DeviceId srcId = srcHost.location().deviceId();
                DeviceId dstId = dstHost.location().deviceId();
                log.trace("SRC ID is {}, DST ID is {}", srcId, dstId);

                cleanFlowRules(sd, egress.deviceId());

                Set<Path> shortestPaths = srcPaths.get(srcId);
                if (shortestPaths == null) {
                    shortestPaths = topologyService.getPaths(topologyService.currentTopology(),
                            egress.deviceId(), srcId);
                    srcPaths.put(srcId, shortestPaths);
                }
                backTrackBadNodes(shortestPaths, dstId, sd);
            }
        }
    }
 
    // Backtracks from link down event to remove flows that lead to blackhole
    private void backTrackBadNodes(Set<Path> shortestPaths, DeviceId dstId, SrcDstPair sd) {
        for (Path p : shortestPaths) {
            List<Link> pathLinks = p.links();
            for (int i = 0; i < pathLinks.size(); i = i + 1) {
                Link curLink = pathLinks.get(i);
                DeviceId curDevice = curLink.src().deviceId();

                // skipping the first link because this link's src has already been pruned beforehand
                if (i != 0) {
                    cleanFlowRules(sd, curDevice);
                }

                Set<Path> pathsFromCurDevice =
                        topologyService.getPaths(topologyService.currentTopology(),
                                                 curDevice, dstId);
                if (pickForwardPathIfPossible(pathsFromCurDevice, curLink.src().port()) != null) {
                    break;
                } else {
                    if (i + 1 == pathLinks.size()) {
                        cleanFlowRules(sd, curLink.dst().deviceId());
                    }
                }
            }
        }
    }
    
    // Selects a path from the given set that does not lead back to the
    // specified port if possible.
    private Path pickForwardPathIfPossible(Set<Path> paths, PortNumber notToPort) {
        Path lastPath = null;
        for (Path path : paths) {
            lastPath = path;
            if (!path.src().port().equals(notToPort)) {
                return path;
            }
        }
        return lastPath;
    }
    
    private Set<FlowEntry> getFlowRulesFrom(ConnectPoint egress) {
        ImmutableSet.Builder<FlowEntry> builder = ImmutableSet.builder();
        flowRuleService.getFlowEntries(egress.deviceId()).forEach(r -> {
            if (r.appId() == appId.id()) {
                r.treatment().allInstructions().forEach(i -> {
                    if (i.type() == Instruction.Type.OUTPUT) {
                        if (((Instructions.OutputInstruction) i).port().equals(egress.port())) {
                            builder.add(r);
                        }
                    }
                });
            }
        });

        return builder.build();
    }
    
    // Returns a set of src/dst MAC pairs extracted from the specified set of flow entries
    private Set<SrcDstPair> findSrcDstPairs(Set<FlowEntry> rules) {
        ImmutableSet.Builder<SrcDstPair> builder = ImmutableSet.builder();
        for (FlowEntry r : rules) {
            MacAddress src = null, dst = null;
            for (Criterion cr : r.selector().criteria()) {
                if (cr.type() == Criterion.Type.ETH_DST) {
                    dst = ((EthCriterion) cr).mac();
                } else if (cr.type() == Criterion.Type.ETH_SRC) {
                    src = ((EthCriterion) cr).mac();
                }
            }
            builder.add(new SrcDstPair(src, dst));
        }
        return builder.build();
    }
    
    // Removes flow rules off specified device with specific SrcDstPair
    private void cleanFlowRules(SrcDstPair pair, DeviceId id) {
        log.trace("Searching for flow rules to remove from: {}", id);
        log.trace("Removing flows w/ SRC={}, DST={}", pair.src, pair.dst);
        for (FlowEntry r : flowRuleService.getFlowEntries(id)) {
            boolean matchesSrc = false, matchesDst = false;
            for (Instruction i : r.treatment().allInstructions()) {
                if (i.type() == Instruction.Type.OUTPUT) {
                    // if the flow has matching src and dst
                    for (Criterion cr : r.selector().criteria()) {
                        if (cr.type() == Criterion.Type.ETH_DST) {
                            if (((EthCriterion) cr).mac().equals(pair.dst)) {
                                matchesDst = true;
                            }
                        } else if (cr.type() == Criterion.Type.ETH_SRC) {
                            if (((EthCriterion) cr).mac().equals(pair.src)) {
                                matchesSrc = true;
                            }
                        }
                    }
                }
            }
            if (matchesDst && matchesSrc) {
                log.trace("Removed flow rule from device: {}", id);
                flowRuleService.removeFlowRules((FlowRule) r);
            }
        }

    }
    
    // Wrapper class for a source and destination pair of MAC addresses
    private final class SrcDstPair {
        final MacAddress src;
        final MacAddress dst;

        private SrcDstPair(MacAddress src, MacAddress dst) {
            this.src = src;
            this.dst = dst;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            SrcDstPair that = (SrcDstPair) o;
            return Objects.equals(src, that.src) &&
                    Objects.equals(dst, that.dst);
        }

        @Override
        public int hashCode() {
            return Objects.hash(src, dst);
        }
    }
}
